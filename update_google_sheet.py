import os
import time
import datetime
from app.mygspread import mygspread
import constent
import functions
from update_config import updateConfig
import requests
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()


# 通过文件 tracking_numbers_with_order_status.txt 找出所有的 buyer cancelled 的订单，
# 通过第一列进行分类，存入字典中。
# 在 google sheet 中查找这些订单。
# 更新 google sheet。
class UpdataGoogleSheet:
	def __init__(self):
		self.config = functions.get_config()
		self.mygspreadObj = mygspread(constent.ROOT_PATH + 'app/config/' + self.config.get('gspread', 'gs_json'))
		ucObj = updateConfig()
		self.google_sheet_keys = ucObj.read_ini()
		self.APP_SCRIPT_URL = self.config.get('app_script', 'api_url')
		self.SELLER_CANCELED_ORDERS = 'seller canceled orders'
		self.BACKUP_SELLER_CANCELED = 'Backup09/05 seller canceled'
		
	def find_buyer_cancelled_from_file(self, filepath):
		result = {}
		with open(filepath, 'r', encoding='utf-8') as f:
			for line in f:
				items = line.rstrip('\n').split('\t')
				if len(items) < 7:
					continue
				key_first10 = items[0]
				order_date = items[1]
				order_id = items[2]
				order_number = items[3]
				order_status = items[6].lower()
				if 'cancelled' not in order_status:
					continue
				print(order_number)
				
				sheet_k = key_first10
				if sheet_k not in result:
					result[sheet_k] = []
				result[sheet_k].append([order_date, order_id, order_number])
		return result

	def search_from_list(self, order_id, order_number, all_values):
		row = 0
		i = 0
	
		for line in all_values:
			i += 1
			print(order_id + '------' + line[1])
			print(order_number + '------' + line[29])
			if order_id == line[1] and order_number == line[29]:

				row = i
				print('find info in row {}'.format(row))
				break
		return row

	def update_row(self, row, sheet_name):
		self.mygspreadObj.set_worksheet(sheet_name)
		self.mygspreadObj.update_cell(row,1,'n')
		self.mygspreadObj.update_cell(row,29,'')
		self.mygspreadObj.update_cell(row,30,'Seller Canceled')
		self.mygspreadObj.update_cell(row,31,'')

	def add_to_sheet(self, sheet_name, row_values,sheet_k):
		attempts = 0
		while attempts < 3:
			try:
				last_row = self.find_last_row(sheet_name)
				print('last row in {} is {}'.format(sheet_name, last_row))
				next_row = last_row + 1
				print('next_row {}'.format(next_row))
				self.mygspreadObj.set_worksheet(sheet_name)
				row_count = self.mygspreadObj.row_count()
				print('row_count {}'.format(row_count))
				if next_row >= row_count:
					print('adding')
					self.mygspreadObj.add_row(100)        
				cell_range = 'A{}:AV{}'.format(next_row,next_row)
				self.mygspreadObj.update_cells2(cell_range, row_values)
				break
			except:
				attempts += 1
				self.check_single_worksheet_exist(sheet_name,sheet_k)
				pass


	def find_first_empty_row(self, sheet_name):
		self.mygspreadObj.set_worksheet(sheet_name)
		all_values = self.mygspreadObj.get_all_values()
		row = 0
		i = 0
		for line in all_values:
			i += 1
			all_empty = all(v == '' for v in line)
			if all_empty == True:
				row = i
				break
		if row == 0:
			row = i + 1
		return row

	def find_last_row(self, sheet_name):
		self.mygspreadObj.set_worksheet(sheet_name)
		all_values = self.mygspreadObj.get_all_values()
		row = len(all_values)
		return row

	def add_date_in_sheet(self, sheet_name):
		self.mygspreadObj.set_worksheet(sheet_name)
		last_row = self.find_last_row(sheet_name)
		print('last_row {}'.format(last_row))
		next_row = last_row + 1
		print('next_row {}'.format(next_row))
		row_count = self.mygspreadObj.row_count()
		print('row_count {}'.format(row_count))    
		if next_row >= row_count:
			print('adding')
			self.mygspreadObj.add_row(100)
		self.mygspreadObj.update_cell(next_row + 1, 1,time.strftime("%d %b"))

	def check_worksheet_exist(self, sheet_name,sheet_k):
		for key in self.google_sheet_keys:
			payload = {'m': 'getWorksheet', 
					   'ss_id': key,
					   'sheet_name': sheet_name }
			try_time = 0
			while True:
				print('try_time {}'.format(try_time))
				try_time += 1
				if try_time > 3:
					break
				try:
					r = requests.get(self.APP_SCRIPT_URL, verify=False, params=payload)
					print(r.text)
					if r.text != 'Sheet':
						payload = {'m': 'createWorksheetByTemplate', 
								   'ss_id': key,
								   'sheet_name': sheet_name,
								   'template_name': 'template' }

						r = requests.get(self.APP_SCRIPT_URL, verify=False, params=payload)
					break
				except Exception as e:
					print(e)
				time.sleep(2)

	def check_single_worksheet_exist(self, sheet_name,sheet_k):
	
		payload = {'m': 'getWorksheet', 
				   'ss_id': sheet_k,
				   'sheet_name': sheet_name }
		try_time = 0
		while True:
			print('try_time {}'.format(try_time))
			try_time += 1
			if try_time > 3:
				break
			try:
				r = requests.get(self.APP_SCRIPT_URL, verify=False, params=payload)
				print(r.text)
				if r.text != 'Sheet':
					payload = {'m': 'createWorksheetByTemplate', 
							   'ss_id': sheet_k,
							   'sheet_name': sheet_name,
							   'template_name': 'template' }

					r = requests.get(self.APP_SCRIPT_URL, verify=False, params=payload)
				break
			except Exception as e:
				print(e)
			time.sleep(2)

	def find_line_from_google(self,data):
		# self.check_worksheet_exist(self.SELLER_CANCELED_ORDERS)
		# self.check_worksheet_exist(self.BACKUP_SELLER_CANCELED)
		sheet_k = ''
		for k, data_list in data.items():            
			for key in self.google_sheet_keys:
				if k == key[0:10]:
					sheet_k = key
					break
			if sheet_k == '':
				return
			print(sheet_k)

			self.mygspreadObj.open_spreadsheet(sheet_k, 'key')
			self.add_date_in_sheet(self.SELLER_CANCELED_ORDERS)
			self.add_date_in_sheet(self.BACKUP_SELLER_CANCELED)
			
			for line in data_list:
				print('data list: {}'.format(line))
				sheet_name = line[0]
				order_id = line[1]
				order_number = line[2]
				self.mygspreadObj.set_worksheet(sheet_name)
				all_values = self.mygspreadObj.get_all_values()
				row = self.search_from_list(order_id, order_number, all_values)
				if row:
					cell_range = "A{}:AV{}".format(row,row)
					row_values = self.mygspreadObj.get_cells_with_formula(cell_range)
					new_row_values = []
					i = 0
					for item in row_values:
						i += 1
						if i == 1:
							new_row_values.append('n')
						elif i == 29:
							new_row_values.append('')
						elif i == 30:
							new_row_values.append('Seller Canceled')
						elif i == 31:
							new_row_values.append('')
						else:
							new_row_values.append(item)

					currentDT = datetime.datetime.now()
					functions.write_to_file(str(currentDT) + '\t' + '\t'.join(line) + '\n','cancelled_order_history.txt','a')

					f = open("cancelled_order_num_history.txt", "r",encoding="utf-8")
					cancelled_list = f.readlines()
					print(order_number)
					print(cancelled_list)
					if (order_number + '\n') not in cancelled_list:
						try:
							print('start to update_sheet')
							self.add_to_sheet(self.BACKUP_SELLER_CANCELED, row_values,sheet_k)
							self.update_row(row, sheet_name)
							self.add_to_sheet(self.SELLER_CANCELED_ORDERS, new_row_values,sheet_k)
						except Exception as e:
							functions.write_to_file(order_number + '\t' + str(e) + '\n', 'error_sheet.txt', 'a')
							pass
					else:
						print(order_number + ' is already uploaded!')
						continue

					functions.write_to_file(order_number + '\n','cancelled_order_num_history.txt','a')




def main():
	ugs = UpdataGoogleSheet()
	cancelled_orders = ugs.find_buyer_cancelled_from_file(constent.ROOT_PATH + 'result/tracking_numbers_with_order_status.txt')
	print('cancelled_orders:')
	print(cancelled_orders)
	if cancelled_orders:
		ugs.find_line_from_google(cancelled_orders)

if __name__ == '__main__':
	start = time.time()
	main()
	end = time.time()
	print(end-start)
