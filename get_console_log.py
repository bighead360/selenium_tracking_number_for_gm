from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import functions


from selenium.webdriver.common.desired_capabilities import DesiredCapabilities    
# enable browser logging
d = DesiredCapabilities.CHROME
d['loggingPrefs'] = { 'browser':'ALL' }
driver = webdriver.Chrome('Drivers/'+functions.get_config().get('chrome','driver'), desired_capabilities=d)
# load some site
driver.get('https://amazon.com')

# self.br = webdriver.Chrome(self.EXECUTEABLE_PATH)
# print messages
print('console log')
for entry in driver.get_log('browser'):
    print('console log')
    print(entry)



