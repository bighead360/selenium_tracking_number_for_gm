import os
import time
import re
import random
import traceback
import inspect
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import ctypes
import pickle
import constent
import functions
from parsel import Selector


class FindTracking:
    user = ''
    password = ''
    region = 'amazon.com'

    def __init__(self):
        self.config = functions.get_config()
        self.userinfo = self.find_account_info()
        self.find_urls()
        self.EXECUTEABLE_PATH = constent.ROOT_PATH + 'Drivers/' + self.config.get('chrome','driver')
        self.TRACKING_FILES = functions.all_tracking_files()
        
    def prapare_env(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--profile-directory=Profile 1')
        options.add_argument(r'user-data-dir=C:/dev/chrome_profile/{}'.format(constent.ROOT_FOLDER))
        self.br = webdriver.Chrome(executable_path=self.EXECUTEABLE_PATH, options=options)
        self.action = webdriver.ActionChains(self.br)       
    
    def find_account_info(self):
        self.MACHINE_NAME = self.config.get('email','machine_name')
        self.CC_TO = self.config.get('email','cc_to').replace(' ', '').split(',')
        
    def find_urls(self):
        self.BASE_URL = {}
        self.BASE_URL['amazon.com'] = self.config.get('url us','buyer_base_url')
        self.BASE_URL['amazon.co.uk'] = self.config.get('url uk','buyer_base_url')
        self.ORDERS_LISTING_URL = {}
        self.ORDERS_LISTING_URL['amazon.com'] = self.BASE_URL['amazon.com'] + 'gp/css/order-history/ref=nav_youraccount_orders'
        self.ORDERS_LISTING_URL['amazon.co.uk'] = self.BASE_URL['amazon.co.uk'] + 'gp/css/order-history/ref=nav_youraccount_orders'
        self.ORDER_HISTORY_URL = {}
        self.ORDER_HISTORY_URL['amazon.com'] = self.BASE_URL['amazon.com'] + 'gp/your-account/order-history?ref_=ya_d_c_yo'
        self.ORDER_HISTORY_URL['amazon.co.uk'] = self.BASE_URL['amazon.co.uk'] + 'gp/your-account/order-history?ref_=ya_d_c_yo'
 
    def check_captcha(self,):
        len_typing_letters = 0
        count = 0
        while(len_typing_letters < 6):
            count += 1
            if count > 100:
                exit()
            try:
                captcha_text_elem = self.br.find_element_by_id('auth-captcha-guess')
                print("captcha needed in auth-captcha-guess")
                len_typing_letters = len(captcha_text_elem.get_attribute('value'))
                print('Typing captha {} letters.'.format(len_typing_letters))
                time.sleep(3)
            except:
                len_typing_letters = 6
                print("no captcha needed.")

    def search_for_tracking(self, region, order_number):
        tracking_info = ''
        tracking_provided = ''
        shipped_with = ''
        tracking_status = ''
        url = 'https://www.{}/progress-tracker/package/ref=scr_pt_track?_encoding=UTF8&from=gp&itemId=&orderId={}&packageIndex=0&shipmentId=6000000000000'.format(region, order_number)
        try_time = 0
        while True:
            try_time += 1
            if try_time > 3:
                input("there is something wrong when getting url, line 83.")
                break
            try:
                self.br.get(url)
                break
            except Exception as e:
                print(e)
                time.sleep(30)

        self.check_captcha()
        print(url)
        time.sleep(3)
        html_content = self.br.page_source
        # pattern = re.compile(r'<div id="carrierRelatedInfo-container"[^>]*?>([\s\S]*?)</div>')
        # match = re.search(pattern, html_content)

        # selector = Selector(html_content)
        # match = selector.xpath('//div[@id="tracking-events-container"]/text()')
        # print(match)
        if html_content:
            tracking_provided = self.get_tracking_provided(html_content)
            shipped_with = self.get_shipped_with(html_content)
            tracking_number = self.get_tracking_number(html_content)
            tracking_info = '{}:{}'.format(shipped_with, tracking_number)
        tracking_status = self.get_tracking_status(html_content)
        return tracking_info, tracking_provided, shipped_with, tracking_status

    def get_tracking_provided(self, html_content):
        result = ''
        pattern = re.compile(r'Tracking info provided by ([^<]*?)<')
        match = re.search(pattern, html_content)
        if match:
            result = match.group(1).strip()
        if result == '':
            pattern = re.compile(r'Delivery by ([^<]*?)<')
            match = re.search(pattern, html_content)
            if match:
                result = match.group(1).strip()
        return result

    def get_shipped_with(self, html_content):
        result = ''
        pattern = re.compile(r'Shipped with ([^<]*?)<')
        match = re.search(pattern, html_content)
        if match:
            result = match.group(1).strip()
        if result == '':
            pattern = re.compile(r'Delivery by ([^<]*?)<')
            match = re.search(pattern, html_content)
            if match:
                result = match.group(1).strip()
            else:
                result = 'Other'
        return result

    def get_tracking_status(self, html_content):
        result = ''
        pattern = re.compile(r'<span id="primaryStatus"[^>]*?>([\s\S]*?)</div>')
        match = re.search(pattern, html_content)
        if match:
            result = functions.remove_tags(match.group(1)).strip()
        return result

    def get_tracking_number(self, html_content):
        result = ''
        pattern = re.compile(r'Tracking ID: ([^<]*?)<')
        match = re.search(pattern, html_content)
        if match:
            result = match.group(1).strip()
        print('tracking_number is ' + result)
        return result   

    def quit_browser(self):
        try:
            self.br.quit()
        except Exception as e:
            print(e)

    def record_exce(self, errmsg):
        with open(constent.ROOT_PATH + 'runtime_error.log.', 'a', encoding='utf-8') as f:
            f.write(time.strftime("%Y-%m-%d %H:%M:%S")+'\n')
            f.write(errmsg)
            f.write('\n=================================================\n')
        
def main(mode=''):
    ftracking = FindTracking()
    count_file = '{}count.txt'.format(functions.get_path(['result']))
    count = 0
    if os.path.isfile(count_file) == True:
        count_list = functions.read_file(count_file)
        print(count_list)
        count = int(count_list[0])
    orderid_file = '{}orderid.txt'.format(functions.get_path(['app','config']))
    
    gspread_orders = functions.read_file(orderid_file)
    if count == 0:
        for k,v in ftracking.TRACKING_FILES.items():
            functions.reset_file(v)
    orig_count = count
    print('orig count: {}'.format(orig_count))
    
    i = 0
    j = 1
    ftracking.prapare_env()
    order_count = len(gspread_orders)
           
    for gs_order in gspread_orders:
        i += 1
        print('{}/{}/{}'.format(i, order_count, j))
        if i <= orig_count:
            continue

        gs_order = gs_order.rstrip('\n')
        key_first10, orderdate, orderid, order_number, account, store_url, remark, sales_channel = gs_order.split('\t')
        if orderid == 'order-id':
            print('We will skip this order:{} order_number:{}'.format(orderid, order_number))
            continue
        if 'Seller' in order_number:
            print('We will skip this order:{} order_number:{}'.format(orderid, order_number))
            continue
        if 'Canceled' in order_number:
            print('We will skip this order:{} order_number:{}'.format(orderid, order_number))
            continue
        if order_number == '':
            print('The order number is empty.')
            continue
        if ' FWD' in remark.upper():
            print('The order is FWD.')
            continue
        remark_0_12 = remark[0:12].upper()
        if remark_0_12 == 'UK SHIPMENT.':
            region = 'amazon.co.uk'
        elif remark_0_12 == 'US SHIPMENT.':
            region = 'amazon.com'
        else:
            region = sales_channel.lower()

        j += 1
        sleep_time = random.randrange(10, 20)
        if j % 10 == 0:
            print('sleep for {} seconds'.format(sleep_time))
            time.sleep(sleep_time)

        sleep_time = random.randrange(60, 80)
        if j % 50 == 0:
            print('sleep for {} seconds'.format(sleep_time))
            time.sleep(sleep_time)

        print('We are working on order:{} {} {} {} {}'.format(key_first10, orderdate, orderid, order_number, account))
        tracking_info, tracking_provided, shipped_with, tracking_status = ftracking.search_for_tracking(region, order_number)
        functions.write_to_file(i, count_file, 'w')
        print('current count: {}'.format(i))
        content_line = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(key_first10, orderdate, orderid, order_number, account, tracking_info, tracking_status, tracking_provided, store_url, shipped_with, remark_0_12, sales_channel)
        functions.write_to_file(content_line, ftracking.TRACKING_FILES['tracking_num_wos'], 'a')
        if tracking_info == '':
            print('We will skip this order:{} order_number:{} because tracking_info is empty'.format(orderid, order_number))
            continue
        if 'other:' in tracking_info:
            print('We will skip this order:{} order_number:{} because tracking_info is other'.format(orderid, order_number))
            continue
        tracking_url = ''
        content_line = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(key_first10, orderid, order_number, account, tracking_info, tracking_url, store_url)
        functions.write_to_file(content_line, ftracking.TRACKING_FILES['tracking_num'], 'a')
        print(tracking_info)

        time.sleep(5)
    ftracking.quit_browser()
    time.sleep(3)

    tracking_result = functions.read_file(ftracking.TRACKING_FILES['tracking_num_wos'])
    for line in tracking_result:
        items = line.split('\t')
        if len(items) < 7:
            try:
                print(line)
            except Exception as e:
                print(e)
            continue
        order_status = items[6].lower()
        if 'your action required' in order_status or 'delayed' in order_status or 'return' in order_status or 'cancelled' in order_status:
            print(order_status)
            functions.write_to_file(line, ftracking.TRACKING_FILES['tracking_num_ar'], 'a')

    functions.write_to_file(0, count_file, 'w')
    print('Done')
    time.sleep(3)

if __name__ == '__main__':
    main()
