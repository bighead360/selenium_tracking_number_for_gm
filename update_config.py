import requests
import json
import re
import configparser
import constent
import functions
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()


class updateConfig:
    def __init__(self):
        self.config = functions.get_config()
        self.root_path = constent.ROOT_PATH
        self.googlesheets_file = self.root_path + 'app/config/googlesheets.ini'
        self.APP_SCRIPT_URL = self.config.get('app_script', 'api_url')
        self.FOLDER_ID_LIST = self.config.get('app_script', 'fd_id')

    def get_files(self):
        url = self.APP_SCRIPT_URL
        folder_list = self.FOLDER_ID_LIST.split(',')
        new_files_list = {}
        for folder in folder_list:
            payload = {'m': 'listFilesInsideFolder', 
                       'fd_id': folder,
                       }

            r = requests.get(url, verify=False, params=payload)
            # with open('test20180308.txt', 'w', encoding='utf-8') as f:
            #     f.write(r.text)
            # exit()
            files_list = json.loads(r.text)
            
            for file in files_list:
                file_name = file[0].strip()
                file_id = file[1].strip()
                if self.check_file_name_code(file_name) == False:
                    continue
                if self.check_file_name_pattern(file_name) == False:
                    continue
                new_files_list[file_name] = file_id
        return new_files_list

    def check_file_name_code(self, filename):
        result = True
        try:
            with open(constent.ROOT_PATH + 'temp_filename_checking.txt', 'w') as f:
                f.write(filename)
        except Exception as e:
            print(e)
            result = False
        return result

    def check_file_name_pattern(self, filename):
        result = True
        if filename[0:3].upper() != 'ACC':
            try:
                print('This file {} is not about order.'.format(filename))
            except Exception as e:
                print(e)
            result = False
        if filename[-6:].lower() == 'closed':
            try:
                print('This file {} is closed.'.format(filename))
            except Exception as e:
                print(e)
            result = False
        if '_All' not in filename:
            result = False
        return result

    def write_ini(self, new_files_list):
        config = configparser.ConfigParser()
        config['DEFAULT'] = new_files_list
        with open(self.googlesheets_file, 'w') as configfile:
            config.write(configfile)

    def read_ini(self):
        result = []
        config = configparser.ConfigParser()
        config.read(self.googlesheets_file)        
        for k,v in config['DEFAULT'].items():
            result.append(v)
        return result

def main():
    uc = updateConfig()
    files = uc.get_files()
    uc.write_ini(files)

if __name__ == '__main__':
    main()
