import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import configparser
import functions
from send_email import SendEmail


def get_email_from_order():
    all_emails = {}
    orderid_file = functions.get_path(['app','config']) + 'orderid.txt'
    with open(orderid_file, 'r', encoding='utf-8') as f:
        for line in f:
            items = line.strip().split('\t')
            buyer_account = items[4]
            if buyer_account == '':
                continue
            all_emails[items[4]] = 1
    return all_emails

def check_email_not_on_record(config_user, all_emails):
    result = {}
    for k,v in all_emails.items():
        if k not in config_user:
            result[k] = 1
    return result

def main():
    config = functions.get_config()
    users_us = config.get('account us','buyer_account').strip().split(' ')
    users_uk = config.get('account uk','buyer_account').strip().split(' ')
    USER = users_us + users_uk
    

    MACHINE_NAME = config.get('email','machine_name')

    all_emails = get_email_from_order()
    result = check_email_not_on_record(USER, all_emails)
    if result:
        message = ''
        for k,v in result.items():
            message += k + '\n'

        semail = SendEmail()
        cc_to = []
        semail.send_email(MACHINE_NAME, 'Selenium tracking number - need to add user in config', message, cc_to)
        
if __name__ == '__main__':
    main()