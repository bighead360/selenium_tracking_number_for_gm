import re
import functions


def check_order_number(order_number):
    result = False
    pattern = re.compile(r'[\d]{3}-[\d]{7}-[\d]{7}')
    match = re.search(pattern, order_number)
    if match:
        result = True
    return result

def main():
    result_msg = ''
    total_orders_path = functions.get_path(['app','config']) + 'orderid.txt'
    all_orders = {}
    with open(total_orders_path, 'r', encoding='utf-8') as f:
        for line in f:
            if 'total' in all_orders:
                all_orders['total'] += 1
            else:
                all_orders['total'] = 1
            items = line.strip().split('\t')
            orderid = items[2]
            order_number = items[3]
            if order_number != '' and check_order_number(order_number) == True:
                if 'has_order_number' in all_orders:
                    all_orders['has_order_number'] += 1
                else:
                    all_orders['has_order_number'] = 1

    result_msg = '{}Orders total: {}\n'.format(result_msg, all_orders['total'])
    result_msg = '{}Orders has order number: {}\n'.format(result_msg, all_orders['has_order_number'])
    result_msg = '{}{}/{} {}\n'.format(result_msg, all_orders['has_order_number'],all_orders['total'], (all_orders['has_order_number']/all_orders['total']))


    tracking_result_path = functions.get_path(['result']) + 'tracking_numbers.txt'
    with open(tracking_result_path, 'r', encoding='utf-8') as f:
        for line in f:
            if 'has_tracking_number' in all_orders:
                all_orders['has_tracking_number'] += 1
            else:
                all_orders['has_tracking_number'] = 1

    result_msg = '{}Orders has tracking number: {}\n'.format(result_msg, all_orders['has_tracking_number'])
    result_msg = '{}{}/{} {}\n'.format(result_msg, all_orders['has_tracking_number'],all_orders['has_order_number'], (all_orders['has_tracking_number']/all_orders['has_order_number']))

    # print(result_msg)

    tracking_statistics_path = functions.get_path(['result']) + 'tracking_statistics.txt'
    functions.write_to_file(result_msg, tracking_statistics_path, 'w')


if __name__ == '__main__':
    main()