import os
import time
import hashlib
import json
import requests
from urllib.parse import quote
import constent
import functions


class TrackingOperateDb:
    def __init__(self):
        self.config = functions.get_config()
        self.root_path = constent.ROOT_PATH
        self.token = self.config.get('db','token')
        self.api_tracking = self.config.get('db','api_tracking')
        print(self.api_tracking)

    def get_storeid(self):
        payload = {}
        payload['store_table'] = 'stores'
        payload['store_action'] = 'select'
        # payload['order_by'] = 'id desc'
        payload['secret_key'] = self.signature_to(payload['store_table'])
        print(self.api_tracking)
        print(payload)
        r = requests.post(self.api_tracking, data=payload)
        if r.status_code == 200:
            functions.write_to_file(r.text.strip(), self.root_path + 'stores.txt', 'w')
            print('Successfully stored the store id.')

    def organize_stores(self):
        with open(self.root_path + 'stores.txt', 'r', encoding='utf-8') as f:
            json_str = f.read()
        all_stores = {}
        stores = json.loads(json_str)
        for store in stores:
            store_name = store['name']
            if 'https://' in store_name:
                store_name = store_name[8:]
            elif 'http://' in store_name:
                store_name = store_name[7:]
            all_stores[store_name] = store['id']
        return all_stores

    def add_tracking(self):
        all_stores = self.organize_stores()
        new_stores = {}
        with open(self.root_path + 'result/tracking_numbers.txt', 'r', encoding='utf-8') as f:
            for line in f:
                items = line.strip().split('\t')
                if len(items) < 7:
                    continue

                orderid = quote(items[1])
                print(orderid)
                tracking = quote(items[4])
                print('tracking is  ' + tracking)
                pair = tracking.split('%3A')

                if pair[0] == '' or pair[1] == '':
                    print(orderid + ' is no tracking info. no upload')
                    continue 
                store_url = quote(items[6])
                print(store_url)
                if store_url not in all_stores:
                    new_stores[store_url] = 1
                    print('cannot find the store' + store_url )
                    continue
                store_id = all_stores[store_url]
                print('added ' + orderid)

                payload = {}
                payload['store_table'] = 'tracking'
                payload['store_action'] = 'put'                
                payload['where[store_id]'] = store_id
                payload['where[orderid]'] = orderid
                payload['append[tracking_info]'] = tracking
                # payload['data[tracking_info]'] = tracking

                sig_where = {'orderid':orderid, 'store_id':store_id}
                sorted_sig_where_keys = sorted(sig_where.keys())
                sig_where_list = []
                for i in sorted_sig_where_keys:
                    sig_where_list.append(sig_where[i])

                payload['secret_key'] = self.signature_to('{}{}'.format(payload['store_table'], '-'.join(sig_where_list)))
                r = requests.post(self.api_tracking, data=payload)
                time.sleep(0.2)

        print(new_stores.keys())
        functions.write_to_file('\n'.join(new_stores.keys())+'\n', self.root_path + 'new_stores.txt', 'w')

    def fetch_tracking(self, orderid):
        payload = {}
        payload['store_table'] = 'tracking'
        payload['store_action'] = 'select'
        payload['where[orderid]'] = orderid
        payload['secret_key'] = self.signature_to('{}{}'.format(payload['store_table'], orderid))
        r = requests.post(self.api_tracking, data=payload)
        print(orderid)
        print(r.text)

    def signature_to(self, data_str = ''):
        tomorrow  = time.strftime('%Y-%m-%d', time.localtime(time.time() + 24*3600))
        encode_str = '{}{}'.format(data_str, tomorrow).encode('utf-8')
        signature = hashlib.sha1(encode_str)
        first_signature = signature.hexdigest()
        rev_first = first_signature[::-1]
        encode_str = '{}{}'.format(rev_first, self.token).encode('utf-8')
        signature = hashlib.md5(encode_str)
        second_signature = signature.hexdigest()
        return second_signature.upper()

def main():
    tod = TrackingOperateDb()
    # tod.signature_to('abcd')
    tod.get_storeid()
    tod.add_tracking()
    # tod.fetch_tracking('#TBK-4866')


if __name__ == '__main__':
    main()