import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import functions


class SendEmail:
    def __init__(self):
        self.config = functions.get_config()
        self.send_from = self.config.get('email','send_from')
        self.send_to = self.config.get('email','send_to').replace(' ', '').split(',')
        self.login_pass = self.config.get('email','login_pass')

    def send_email(self, machine_name, title, message, cc_to = []):
        outer = MIMEMultipart()
        outer['From'] = self.send_from
        outer['To'] = ', '.join(self.send_to)
        outer['CC'] = ', '.join(cc_to)
        outer['Subject'] = 'MN{}:{}'.format(machine_name, title)
        
        msg = MIMEText(message)
        outer.attach(msg)
        composed = outer.as_string()

        mailserver = smtplib.SMTP('smtp.gmail.com',587)
        # identify ourselves to smtp gmail client
        mailserver.ehlo()
        # secure our email with tls encryption
        mailserver.starttls()
        # re-identify ourselves as an encrypted connection
        mailserver.ehlo()
        mailserver.login(self.send_from, self.login_pass)
        mailserver.sendmail(self.send_from, self.send_to + cc_to, composed)

        mailserver.quit()

    def send_email_with_attachment(self, machine_name, title, filepath, cc_to = []):
        if os.path.isfile(filepath) == False:
            print('The file {} does not exist.'.format(filepath))
            return
            
        outer = MIMEMultipart()
        outer['From'] = self.send_from
        outer['To'] = ', '.join(self.send_to)
        outer['CC'] = ', '.join(cc_to)
        outer['Subject'] = 'MN{}:{}'.format(machine_name, title)
        # print(outer)
        # print(outer.as_string())
        # exit()
        
        # if os.path.isfile(filepath) == False:
        #     print('The file {} does not exist.'.format(filepath))
        #     return

        if os.stat(filepath).st_size > 0:
            with open(filepath, 'r', encoding='utf-8') as fp:
                msg = MIMEText(fp.read())

            directory, filename = os.path.split(filepath)
            msg.add_header('Content-Disposition', 'attachment', filename=filename)
            outer.attach(msg)
            composed = outer.as_string()

            mailserver = smtplib.SMTP('smtp.gmail.com',587)
            # identify ourselves to smtp gmail client
            mailserver.ehlo()
            # secure our email with tls encryption
            mailserver.starttls()
            # re-identify ourselves as an encrypted connection
            mailserver.ehlo()
            mailserver.login(self.send_from, self.login_pass)
            mailserver.sendmail(self.send_from, self.send_to + cc_to, composed)

            mailserver.quit()

