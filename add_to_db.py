import os
import requests
from urllib.parse import quote
import hashlib
import traceback
import json
import time
import constent
import functions


# 步骤如下：
# 1. 读取 tracking number 文件
# 2. 将 request 发送到远程 server
# 3. 得到回复
class AddToDB:
    def __init__(self):
        self.config = functions.get_config()

    def fetch_from_server(self, orderid, carrier_and_number):
        result = ''
        params = {}
        params['orderid'] = quote(orderid)
        params['accesskey'] = self.config.get('db','ACCESSKEY')

        try_time = 0
        print(orderid)
        while True:
            error_msg = ''
            try_time += 1
            if try_time == 3:
                print('tried 3 times')
                break
            try:
                response = requests.get(self.config.get('db','link') + 'find_tracking.php', params = params)                
                resjson = json.loads(response.text)
                if resjson == False:
                    print('json file error.')
                    break                
                if resjson['tracking_info'] != '' and carrier_and_number in resjson['tracking_info']:
                    result = 'equals'
                else:
                    result = 'not equal'
                    print('tracking in db: {} tracking in file: {}'.format(resjson['tracking_info'], carrier_and_number))
                    content = '{}\t{}\t{}\t{}\t{}\t{}\n'.format( \
                        orderid, resjson['tracking_info'], resjson['created_on'], resjson['modified_on'], \
                        carrier_and_number, time.strftime('%Y-%m-%d %H:%M:%S') \
                        )
                    functions.write_to_file(content, constent.ROOT_PATH + 'result/tracking_numbers_changed.txt', 'a')
                time.sleep(0.3)               
                break
            except Exception as e:
                functions.record_exce(traceback.format_exc())
                error_msg = traceback.format_exc()
                time.sleep(3)
            try:
                print(error_msg)
            except Exception as e:
                print(str(e))
        return result

    def add_to_server(self, tracking_info):
        try_time = 0
        while True:
            error_msg = ''
            try_time += 1
            if try_time == 3:
                break
            try:
                response = requests.get(self.config.get('db','link') + 'add_tracking.php', params = tracking_info)
                # print(response.text)
                time.sleep(0.3)
                break
            except Exception as e:
                functions.record_exce(traceback.format_exc())
                error_msg = traceback.format_exc()
                time.sleep(3)
            try:
                print(response.text)
                print(error_msg)
            except Exception as e:
                print(str(e))


def main():
    tracking_num_file = functions.get_path(['result']) + 'tracking_numbers.txt'
    tracking_info = {}
    md5_str = ''
    atd = AddToDB()
    secret_key = atd.config.get('db','secret_key')
    with open(tracking_num_file, 'r') as f:
        for line in f:
            line = line[:-1]
            items = line.split('\t')
            if len(items) < 7:
                continue
            orderid = items[1]
            carrier_and_number = items[4]          
            tracking_url = items[5]
            store_url = items[6]
            pair = carrier_and_number.split(':')
            if 'http' in carrier_and_number:
                continue
            if pair[0] == '' or pair[1] == '':
                print(orderid + ' is no tracking info. no upload')
                continue 

            if orderid == 'orderid':
                continue

            compare_tracking = atd.fetch_from_server(orderid,carrier_and_number)
            print('compare_tracking is ' + compare_tracking)
            if compare_tracking == 'equals':
                continue

            md5_str = orderid + '|' + carrier_and_number + '|' + tracking_url + '|' + secret_key
            md5_str = md5_str.encode('utf-8')
            hash_md5 = hashlib.md5(md5_str)
            signature = hash_md5.hexdigest()

            tracking_info['orderid'] = quote(orderid)
            tracking_info['carrier_and_number'] = quote(carrier_and_number)
            tracking_info['tracking_url'] = quote(tracking_url)
            tracking_info['signature'] = signature
            tracking_info['store_url'] = store_url
            print('store url is ' + store_url)
            atd.add_to_server(tracking_info)

if __name__ == '__main__':
    main()