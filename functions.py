import os
import re
import time
import configparser
import constent


def get_path(directory_list=[]):
    if not directory_list:
        dir = constent.ROOT_PATH.rstrip('/') + '/'
    else:
        dir = constent.ROOT_PATH.rstrip('/') + '/' + '/'.join(directory_list) + '/'
    if not os.path.exists(dir):
        os.makedirs(dir)
    return dir

def get_config():
    config_file = get_path(['app','config']) + 'config.ini'
    config = configparser.ConfigParser()
    config.read(config_file)
    return config

def record_exce(errmsg):
    with open(constent.ROOT_PATH + 'runtime_error.log', 'a', encoding='utf-8') as f:
        f.write(time.strftime("%Y-%m-%d %H:%M:%S")+'\n')
        f.write(errmsg)
        f.write('\n=================================================\n')

def write_to_file(content, filepath, mode):
    with open(filepath, mode, encoding='utf-8') as f:
        f.write('{}'.format(content))

def read_file(filepath):
    result = ''
    with open(filepath, 'r', encoding='utf-8') as f:
        result = f.readlines()
    return result

def reset_file(filepath):
    with open(filepath, 'w', encoding='utf-8') as f:
        f.write('')

def check_file_exists(filepath):
    if os.path.isfile(filepath) != True:
        with open(filepath, 'w', encoding='utf-8') as f:
            f.write('')

def filter_r_n_t(input_str):
    return input_str.replace('\r', '').replace('\n', '').replace('\t', '')

def remove_tags(text):
    pattern = re.compile(r'<[^>]+>')
    return pattern.sub('', text)

def all_tracking_files(): 
    TRACKING_FILES = {
            'tracking_num':'{}tracking_numbers.txt'.format(get_path(['result'])), 
            'tracking_num_wos':'{}tracking_numbers_with_order_status.txt'.format(get_path(['result'])), 
            'tracking_num_ar':'{}tracking_numbers_action_required.txt'.format(get_path(['result'])), 
            'tracking_num_it':'{}tracking_numbers_in_transit.txt'.format(get_path(['result'])), 
            'tracking_num_dl':'{}tracking_numbers_direct_link.txt'.format(get_path(['result'])), 
            'tracking_num_nts':'{}needto_send.txt'.format(get_path(['result'])),
            'tracking_num_nsc':'{}not_sure_carrier.txt'.format(get_path(['result'])),
            'tracking_num_stat':'{}tracking_statistics.txt'.format(get_path(['result'])),
            'tracking_num_wt':'{}without_tracking.txt'.format(get_path(['result'])),
        }
    return TRACKING_FILES
