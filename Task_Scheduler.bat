@ECHO OFF
REM At first, the program will check the task by its name
REM If the program find the task, it will delete the task and re-create the task again.
REM If the program doesn't find the task, it will create the task.
SET mypath=%~dp0
SET MYDIR=%mypath%
SET MYDIR1=%MYDIR:~0,-1%
for %%f in (%MYDIR1%) do set myfolder=%%~nxf
SET taskname="%myfolder%"
SET taskrun=%mypath%start_checking.bat

schtasks /query /TN %taskname% >NUL 2>&1
if %errorlevel% EQU 0 (
	schtasks /delete /TN %taskname%
	schtasks /create /sc minute /mo 360 /TN %taskname% /TR %taskrun%
) else (
	schtasks /create /sc minute /mo 360 /TN %taskname% /TR %taskrun%
)

