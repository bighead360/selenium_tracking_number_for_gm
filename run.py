import time
import os
import ctypes
import find_orderid
import check_email
import add_to_db
import tracking_operate_db
from send_email import SendEmail
import find_tracking_Chrome
import update_google_sheet
import constent
import functions
import update_config
import statistics_result


def check_program_run(program_name):
    EnumWindows = ctypes.windll.user32.EnumWindows
    EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
    GetWindowText = ctypes.windll.user32.GetWindowTextW
    GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
    IsWindowVisible = ctypes.windll.user32.IsWindowVisible

    titles = []
    def foreach_window(hwnd, lParam):
        if IsWindowVisible(hwnd):
            length = GetWindowTextLength(hwnd)
            buff = ctypes.create_unicode_buffer(length + 1)
            GetWindowText(hwnd, buff, length + 1)
            titles.append(buff.value)
        return True
    EnumWindows(EnumWindowsProc(foreach_window), 0)

    program_running = False
    found_time = 0
    for cmd_title in titles:
        if program_name == cmd_title:
            found_time += 1
        if found_time > 1:
            program_running = True
            print('the program is running.')
            break
    return program_running

def main():
    program_name = '{}.cmd'.format(constent.ROOT_FOLDER)
    if check_program_run(program_name) == True:
        time.sleep(5)
        return
    last_list = functions.read_file('./result/tracking_numbers.txt')
    successed_tracking = functions.read_file('./result/last_successed_tracking.txt')
    for order in last_list:
        attrs = order.split('\t')
        if 'https' in attrs[4]:
            continue
        tracking_sp = attrs[4].split(':')
        if len(tracking_sp) == 2:
            if tracking_sp[1] != '':
                print(order)
                successed_tracking.append(attrs[1] + '\n')
    print(len(successed_tracking))
    successed_tracking = list(dict.fromkeys(successed_tracking))
    functions.write_to_file(('').join(successed_tracking),'./result/last_successed_tracking.txt','w')



    count = 0
    count_file = constent.ROOT_PATH + 'result/count.txt'

    if os.path.isfile(count_file) == True:
        with open(count_file, 'r', encoding='utf-8') as f:
            count_content = f.read()
            try:
                count = int(count_content.strip())
            except Exception as e:
                print(e)
    print('count: {}'.format(count))
    functions.write_to_file(count, count_file, 'w')
    
    if count == 0:
        update_config.main()
        find_orderid.main('recent10days')
        check_email.main()
    find_tracking_Chrome.main()
    # add_to_db.main()
    time.sleep(3)
    # tracking_operate_db.main()

    statistics_result.main()

    semail = SendEmail()
    config = functions.get_config()
    cc_to = config.get('email','cc_to').replace(' ', '').split(',')
    machine_name = config.get('email','machine_name')
    print('cc to {}'.format(','.join(cc_to)))
    print(machine_name)
    
    semail.send_email_with_attachment(machine_name, 'Selenium tracking number - tracking numbers | ' + time.strftime("%Y-%m-%d"), constent.ROOT_PATH + 'result/tracking_numbers.txt', cc_to)
    semail.send_email_with_attachment(machine_name, 'Selenium tracking number - tracking numbers with order status | ' + time.strftime("%Y-%m-%d"), constent.ROOT_PATH + 'result/tracking_numbers_with_order_status.txt', cc_to)
    semail.send_email_with_attachment(machine_name, 'Selenium tracking number - tracking numbers action required | ' + time.strftime("%Y-%m-%d"), constent.ROOT_PATH + 'result/tracking_numbers_action_required.txt', cc_to)
    semail.send_email_with_attachment(machine_name, 'Selenium tracking number - not_sure_carrier | ' + time.strftime("%Y-%m-%d"), constent.ROOT_PATH + 'result/not_sure_carrier.txt', cc_to)
    semail.send_email_with_attachment(machine_name, 'Selenium tracking number - tracking statistics | ' + time.strftime("%Y-%m-%d"), constent.ROOT_PATH + 'result/tracking_statistics.txt', cc_to)
    new_stores_path = constent.ROOT_PATH + 'new_stores.txt'
    if os.path.isfile(new_stores_path) == True:
        content = ''
        with open(new_stores_path, 'r', encoding='utf-8') as f:
            content = f.read().strip()
        if content != '':
            semail.send_email_with_attachment(machine_name, 'Selenium tracking number - new stores | ' + time.strftime("%Y-%m-%d"), new_stores_path, cc_to)
    update_google_sheet.main()

if __name__ == '__main__':
    main()