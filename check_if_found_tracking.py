import os
import time

# 程序工作流程：
# 读取第一个文件
# 将第一个文件的第一列提取出来，存入字典。
# 读取第二个文件，在读取每一行的过程中查看是否第一列的值在上面得到的字典中。

# 关于如何使用：
# 比如： 在 file1 中有一些数据，其中有一列是 orderid 列，在 file2 中有一些数据，也有 orderid 列，
#      我想知道在 file2 中有哪些 orderid 是在 file1 中有的，哪些 orderid 是在 file1 中没有的。
#      我的操作就是，把 file1 和 file2 与这个程序放在同一个目录内，运行程序，在命令行输入 file1 的名字，
#      再输入 orderid 是在 file1 的第几列，再输入 file2 的名字，再数据 orderid 是在 file2 的第几列。
#      就可以了，结果文件会生成在同级目录，文件名以 file1 为基础，成为 file1_found.txt, file1_not_found.txt 。

def read_file(inputfile, orderid_col):
    result = {}
    with open(inputfile + '.txt') as f:
        for line in f:
            items = line.rstrip('\n').split('\t')
            orderid = items[orderid_col-1]
            if orderid == 'orderid':
                continue
            result[orderid] = 1
    return result

def find_record(inputfile1, file1_orderid_col, inputfile2, file2_orderid_col):
    file1_orderid_col = int(file1_orderid_col)
    file2_orderid_col = int(file2_orderid_col)
    orderid_dict = read_file(inputfile1, file1_orderid_col)
    print(type(orderid_dict))
    result_file = open(inputfile1 + '_found.txt', 'w', encoding='utf-8')
    with open(inputfile2 + '.txt') as f:
        for line in f:
            items = line.rstrip('\n').split('\t')
            orderid = items[file2_orderid_col-1]
            if orderid == 'orderid':
                continue
            if orderid in orderid_dict:
                result_file.write(line)
                del orderid_dict[orderid]
    result_file.close()

    with open(inputfile1 + '_not_found.txt', 'w', encoding='utf-8') as f:
        for k,v in orderid_dict.items():
            f.write(k + '\n')

def main():
    file1 = input("Please write the file1 name:\n")
    file1_orderid_col = input("Which column is orderid in file1:\n")
    file2 = input("Please write the file2 name:\n")
    file2_orderid_col = input("Which column is orderid in file2:\n")
    find_record(file1, file1_orderid_col, file2, file2_orderid_col)

if __name__ == "__main__":
    start = time.time()
    main()
    end = time.time()
    print(end - start)