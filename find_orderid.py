import os
import re
import time
import requests
from app.mygspread import mygspread
import functions
from update_config import updateConfig
import requests.packages.urllib3
import datetime

requests.packages.urllib3.disable_warnings()


# 步骤如下：
# 1. 根据 key，找到相应的 google sheet 
# 2. 找到 order-id，ORDER NUMBER，ACCOUNT 列
# 3. 将结果写入 app\config\order_numbers.txt 中

def check_orderid(orderid):
    """
    正则规则：
    字符串长度大于等于5
    字符串中不能有空格
    字符串中包含数字
    """
    re_val = False
    if len(orderid) < 5:
        return re_val
    pattern = re.compile(r'[\s]')
    match = re.search(pattern, orderid)
    if match:
        return re_val
    pattern = re.compile(r"[0-9]+")
    match = re.search(pattern, orderid)
    if match:
        print(match.group(0) + ' is a qualified order id!')
        re_val = True
    return re_val

def find_store_url(sku_address):
    result = ''
    if sku_address:
        pattern = re.compile(r'http[:s]{1,2}[/]{2}([^/]*?)/')
        m = re.search(pattern, sku_address)
        if m:
            result = m.group(1)
    return result

def find_order_info(mygspreadObj, sheet_k, options=''):
    if options == '':
        options = 'today'
    mygspreadObj.open_spreadsheet(sheet_k, 'key')
    worksheets = mygspreadObj.get_worksheets_from_sheet_name()
    key_first10 = sheet_k[0:10]
    result = []
    for worksheet in worksheets:

        if re.match('\d{2}/\d{2}', worksheet) is None:
            continue
        ts = datetime.datetime.now()
        try:
            picked_date = datetime.datetime.strptime(worksheet, "%m/%d/%Y")
            during_date = ts - datetime.timedelta(days=7)
        except:
            print(worksheet + " is not valid date formate")
            break
        
       
        if picked_date < during_date:
            print(picked_date.strftime("%Y/%m/%d") + ' is not in consideration!')
            break

        print(picked_date.strftime("%Y/%m/%d") + ' is in consideration!')
        # if options == 'today':
        #     if worksheet != today_sheet:
        #         continue
        # if options == 'recent7days':
        #     if days_count == 8:
        #         break
        # if options == 'recent10days':
        #     print('*********10 days**********')

        #     if days_count == 11:
        #         break
        # if options == 'recent30days':
        #     if days_count == 31:
        #         break
        # if options == 'recent60days':
        #     if days_count == 61:
        #         break

        try:
            mygspreadObj.set_worksheet(worksheet)
        except:
            print('gspread.exceptions.WorksheetNotFound')
            exit()

        all_values = mygspreadObj.get_all_values()
        # print(all_values)
        orderid_key = -1
        # order_num_key = -1
        # account_key = -1
        # sku_address_key = -1
        
        # row_count = 0
        # title_row = -1
        for rows in all_values:
            # row_count += 1
            
            if rows[1] == '':
                continue
            if rows[1] == 'order-id':
                # title_row = row_count
                col_count = 0
                for columns in rows:
                    if columns == 'order-id':
                        orderid_key = col_count
                    if columns == 'ORDER NUMBER':
                        order_num_key = col_count
                    if columns == 'ACCOUNT':
                        account_key = col_count
                    if columns == 'sku address':
                        sku_address_key = col_count
                    if columns == 'Remark':
                        remark_key = col_count
                    if columns == 'Sales Channel':
                        sales_channel_key = col_count
                    
                    col_count += 1
                continue
            if orderid_key != -1:
                if check_orderid(rows[orderid_key]) == False:
                    print('I cannot match the orderid: {}'.format(rows[orderid_key]))
                    continue
                orderid = rows[orderid_key]
                order_number = rows[order_num_key].strip() 
                buyer_email = rows[account_key]
                store_url = find_store_url(rows[sku_address_key])
                remark = functions.filter_r_n_t(rows[remark_key])
                sales_channel = functions.filter_r_n_t(rows[sales_channel_key])
                temp = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(key_first10, worksheet, orderid, order_number, buyer_email, store_url, remark, sales_channel)
                result.append(temp)
    return result


def main(options = ''):
    config_dir = functions.get_path(['app','config'])
    mygspreadObj = mygspread(config_dir + functions.get_config().get('gspread', 'gs_json'))
    
    result_file = config_dir + 'orderid1.txt'
    filter_result_file = config_dir + 'orderid.txt'
    # 将文件内容清空
    functions.reset_file(result_file)
    functions.reset_file(filter_result_file)

    result = []
    filter_result = []
    ucObj = updateConfig()
    google_sheet_keys = ucObj.read_ini()
    
    last_successed_tracking = functions.read_file('./result/last_successed_tracking.txt')
   
    for k in google_sheet_keys:
        result += find_order_info(mygspreadObj, k, options)
    functions.write_to_file('\n'.join(result), result_file, 'w')

    for order in result:
        attrs = order.split('\t')
        order_id = attrs[2] + '\n'
        if order_id not in last_successed_tracking:
            order = order.replace('\n','')
            filter_result.append(order)
        print(attrs[2] + ' already has tracking!')
    functions.write_to_file('\n'.join(filter_result), filter_result_file, 'w')

if __name__ == '__main__':
    main('recent10days')
